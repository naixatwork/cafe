import {
  BadRequestException,
  Body,
  Controller,
  Get, Param,
  Post
} from "@nestjs/common";

import Cafe from "./cafe";

@Controller()
export class AppController {
  private cafeList: Cafe[] = [new Cafe("babel", 2), new Cafe("elan", 5)];

  @Get()
  getAllCafe() {
    return this.cafeList;
  }

  // @Get(":id")
  // getOneCafe(@Param() params: any) {
  //   return params.id
  // }
  @Get(":id")
  getOneCafe(@Param()param: any){
    return param.id
  }

  @Post()
  createNewCafe(@Body() newCafeData: any) {
    if (!newCafeData.name || typeof newCafeData.name !== "string") {
      throw new BadRequestException();
    }
    this.cafeList.push(newCafeData.name);
  }
}
