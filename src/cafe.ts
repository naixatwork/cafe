class Cafe {
  id: string;
  name: string;
  rating: number;

  constructor(newName: string, newRating: number) {
    this.name = newName;
    this.rating = newRating;
    this.id = Math.random().toString();
  }
}

export default Cafe;
